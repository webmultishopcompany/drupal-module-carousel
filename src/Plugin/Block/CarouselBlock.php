<?php
/**
 * @file
 * Contains \Drupal\wmc_carousel\Plugin\Block\CarouselBlock.
 */

namespace Drupal\wmc_carousel\Plugin\Block;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\DatabaseFileUsageBackend;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @Block(
 *   id = "wmc_carousel",
 *   admin_label = @Translation("Carousel"),
 *   category = @Translation("Straw"),
 * )
 */
class CarouselBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ElementInfoManagerInterface $element_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->elementInfo = $element_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('element_info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'slides' => [],
      'theme' => 'carousel',
      'image_style' => NULL,
      'image_style_front' => NULL,
      'arrows' => TRUE,
      'indicators' => FALSE,
      'interval' => 4000,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $is_front = \Drupal::service('path.matcher')->isFrontPage();

    $build = [
      '#theme' => $config['theme'],
      '#arrows' => $config['arrows'],
      '#indicators' => $config['indicators'],
      '#interval' => $config['interval'],
      '#is_front' => $is_front,
      '#cache' => [
        'contexts' => [
          'url.path.is_front'
        ],
      ],
    ];

    if (!empty($config['image_style_front']) && $is_front) {
      $image_style = $config['image_style_front'];
    } else {
      $image_style = $config['image_style'];
    }

    $slides = $config['slides'];

    $file_storage = \Drupal::entityTypeManager()->getStorage('file');

    foreach ($slides as $index => $slide) {
      if (is_array($slide['image'])) {
        $slide['image'] = reset($slide['image']);
      }
      $file = $file_storage->load($slide['image']);
      if (!is_null($file)) {
        $file_uri = $file->getFileUri();

        $image = \Drupal::service('image.factory')->get($file_uri);
        if ($image->isValid()) {
          $width = $image->getWidth();
          $height = $image->getHeight();
        }
        else {
          $width = $height = NULL;
        }

        $build['#slides'][$index]['image'] = [
          'image' => [
            '#theme' => 'image_style',
            '#width' => $width,
            '#height' => $height,
            '#style_name' => $image_style,
            '#uri' => $file_uri,
            '#alt' => $slide['alt'],
          ],
        ];
      }
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $max_file_size = Bytes::toInt('3MB');
    $max_php_file_size = file_upload_max_size();
    $managed_file_info = $this->elementInfo->getInfo('managed_file');

    $slide_count = $form_state->get('slide_count');
    if (is_null($slide_count)) {
      if (isset($config['slides'])) {
        $slide_count = count($config['slides']);
      }
      if ($slide_count == 0) {
        $slide_count = 1;
      }
      $form_state->set('slide_count', $slide_count);
    }

    $form['theme'] = [
      '#title' => $this->t('Theme'),
      '#type' => 'select',
      '#default_value' => $config['theme'],
      '#options' => [
        'carousel' => $this->t('Default'),
        'bootstrap_carousel' => $this->t('Bootstrap'),
      ],
    ];

    $form['arrows'] = [
      '#title' => $this->t('Show arrows'),
      '#type' => 'select',
      '#default_value' => $config['arrows'],
      '#options' => [
        TRUE => $this->t('Yes'),
        FALSE => $this->t('No'),
      ],
    ];

    $form['indicators'] = [
      '#title' => $this->t('Show indicators'),
      '#type' => 'select',
      '#default_value' => $config['indicators'],
      '#options' => [
        TRUE => $this->t('Yes'),
        FALSE => $this->t('No'),
      ],
    ];

    $form['interval'] = [
      '#title' => $this->t('Interval'),
      '#type' => 'textfield',
      '#default_value' => $config['interval'],
    ];

    $form['image_style'] = [
      '#title' => t('Image style'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $config['image_style'],
      '#options' => image_style_options(FALSE),
    ];

    $form['image_style_front'] = [
      '#title' => t('Front page image style'),
      '#description' => t('Use different image style for front page. If set to none, default image style will be used.'),
      '#type' => 'select',
      '#default_value' => $config['image_style_front'],
      '#options' => image_style_options(),
    ];

    $form['slides'] = [
      '#tree' => TRUE,
      '#type' => 'table',
      '#prefix' => '<div id="slides-wrapper">',
      '#suffix' => '</div>',
      '#header' => [
        'label' => '',
        'weight' => $this->t('Weight'),
        'image' => $this->t('Image') . '*',
        'alt' => $this->t('Alt') . '*',
        'operations' => $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];

    $form['add_slide'] = [
      '#type' => 'actions',
      'add_slide' => [
        '#type' => 'submit',
        '#value' => t('Add slide'),
        '#submit' => [get_class($this) . '::addSlide'],
        '#ajax' => [
          'callback' => get_class($this) . '::slideAjaxCallback',
          'wrapper' => 'slides-wrapper',
        ],
      ],
    ];

    $slides_deleted = $form_state->get('slides_deleted');
    if (is_null($slides_deleted)) {
      $slides_deleted = [];
    }

    for ($i = 0; $i < $slide_count; $i ++) {
      if (in_array($i, $slides_deleted) == TRUE) {
        continue;
      }

      $form['slides'][$i]['#attributes']['class'][] = 'draggable';

      $form['slides'][$i]['#weight'] = isset($config['slides'][$i]['#weight']) ? $config['slides'][$i]['#weight'] : 1;

      // Needed for tabledrag to work.
      $form['slides'][$i]['label'] = [
        '#markup' => '',
      ];

      $form['slides'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => isset($config['slides'][$i]['#weight']) ? $config['slides'][$i]['#weight'] : 1,
        '#attributes' => ['class' => ['weight']],
      ];

      $form['slides'][$i]['image']['fid'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Image'),
        '#title_display' => 'invisible',
        '#default_value' => isset($config['slides'][$i]['image']) ? [$config['slides'][$i]['image']] : NULL,
        '#required' => TRUE,
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg'],
          'file_validate_size' => [min($max_file_size, $max_php_file_size)],
        ],
        '#process' => array_merge($managed_file_info['#process'], [[get_class($this), 'processImage']]),
        '#upload_location' => 'public://carousel',
      ];

      $form['slides'][$i]['image']['help'] = [
        '#type' => 'container',
        'text' => [
          '#theme' => 'file_upload_help',
          '#weight' => 20,
          '#description' => '',
          '#upload_validators' => $form['slides'][$i]['image']['fid']['#upload_validators'],
        ],
      ];

      $form['slides'][$i]['alt'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Alt'),
        '#title_display' => 'invisible',
        '#size' => 20,
        '#required' => TRUE,
        '#default_value' => isset($config['slides'][$i]['alt']) ? $config['slides'][$i]['alt'] : NULL,
      ];

      $form['slides'][$i]['operations'] = [];

      if ($slide_count - count($slides_deleted) > 1) {
        $form['slides'][$i]['operations']['remove'] = [
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#name' => 'remove_slide_' . $i,
          '#submit' => [get_class($this) . '::removeSlide'],
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => get_class($this) . '::slideAjaxCallback',
            'wrapper' => 'slides-wrapper',
          ],
          '#attributes' => [
            'slide_key' => $i,
          ],
        ];
      }
    }

    $form_state->setCached(FALSE);

    return $form;
  }

  /**
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $form
   *
   * @return mixed
   */
  public static function processImage($element, FormStateInterface $form_state, $form) {
    $item = $element['#value'];
    $item['fids'] = $element['fids']['#value'];

    // Add the image preview.
    if (!empty($element['#files'])) {
      $file = reset($element['#files']);

      $variables = [
        'style_name' => 'thumbnail',
        'uri' => $file->getFileUri(),
      ];

      // Determine image dimensions.
      if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
        $variables['width'] = $element['#value']['width'];
        $variables['height'] = $element['#value']['height'];
      }
      else {
        $image = \Drupal::service('image.factory')->get($file->getFileUri());
        if ($image->isValid()) {
          $variables['width'] = $image->getWidth();
          $variables['height'] = $image->getHeight();
        }
        else {
          $variables['width'] = $variables['height'] = NULL;
        }
      }

      $element['preview'] = [
        '#type' => 'container',
        '#attributes' => [
          // @todo Remove inline style.
          'style' => 'float: left; margin-right: 10px; min-width: 100px; min-height: 100px',
        ],
        'image' => [
          '#weight' => -10,
          '#theme' => 'image_style',
          '#width' => $variables['width'],
          '#height' => $variables['height'],
          '#style_name' => $variables['style_name'],
          '#uri' => $variables['uri'],
        ],
      ];
    }

    return $element;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function slideAjaxCallback(array &$form, FormStateInterface $form_state) {
    return $form['settings']['slides'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addSlide(array &$form, FormStateInterface $form_state) {
    $slide_count = $form_state->get('slide_count');
    $form_state->set('slide_count', $slide_count + 1);
    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function removeSlide(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (isset($triggering_element['#attributes']['slide_key'])) {
      $slide_key = $triggering_element['#attributes']['slide_key'];

      $slides_deleted = $form_state->get('slides_deleted');
      if (is_null($slides_deleted)) {
        $slides_deleted = [];
      }

      $slides_deleted[] = $slide_key;
      $form_state->set('slides_deleted', $slides_deleted);

      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    $slides_deleted = $form_state->get('slides_deleted');
    if (is_null($slides_deleted)) {
      $slides_deleted = [];
    }
    
    foreach ($form_state->getValues() as $key => $value) {
      if ($key != 'slides') {
        $this->setConfigurationValue($key, $value);
      }
      else {
        $slides_to_save = [];

        foreach ($value as $slide_key => $slide_value) {
          if ($slide_key !== 'actions' && $slide_key !== 'add_slide') {
            if (in_array($slide_key, $slides_deleted) === FALSE) {
              $slide_to_save = [
                'weight' => $slide_value['weight'],
                'alt' => $slide_value['alt'],
              ];

              if ($this->saveFile(reset($slide_value['image']['fid']), 'carousel', 'settings')) {
                $slide_to_save['image'] = reset($slide_value['image']['fid']);
              }

              $slides_to_save[] = $slide_to_save;
            }
          }
        }

        $config = $this->getConfiguration();
        foreach ($slides_deleted as $i) {
          if (isset($config['slides'][$i]['image'])) {
            $this->deleteFile($config['slides'][$i]['image'], 'carousel', 'settings');
          }
        }

        $this->setConfigurationValue($key, $slides_to_save);
      }
    }
  }

  /**
   * @param $fid
   * @param $module_name
   * @param $file_type
   *
   * @return bool
   */
  protected function saveFile($fid, $module_name, $file_type) {
    /** @var File $file */
    $file = File::load($fid);
    if (!is_null($fid)) {
      if ($file->isTemporary()) {
        /** @var DatabaseFileUsageBackend $file_usage */
        $file_usage = \Drupal::service('file.usage');
        $file_usage->add($file, $module_name, $file_type, 1);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param $fid
   * @param $module_name
   * @param $file_type
   *
   * @return bool
   */
  protected function deleteFile($fid, $module_name, $file_type) {
    /** @var File $file */
    $file = File::load($fid);
    if (!is_null($fid)) {
      /** @var DatabaseFileUsageBackend $file_usage */
      $file_usage = \Drupal::service('file.usage');
      $file_usage->delete($file, $module_name, $file_type, 1);
      return TRUE;
    }
    return FALSE;
  }
}
